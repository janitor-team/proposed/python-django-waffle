Source: python-django-waffle
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Michael Fladischer <fladi@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 python3-all,
 python3-django,
 python3-django-jinja,
 python3-mock,
 python3-setuptools,
 python3-sphinx,
Standards-Version: 4.5.0
Homepage: https://github.com/django-waffle/django-waffle/
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-django-waffle
Vcs-Git: https://salsa.debian.org/python-team/packages/python-django-waffle.git
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-python

Package: python-django-waffle-doc
Section: doc
Architecture: all
Depends:
 libjs-mathjax,
 ${misc:Depends},
 ${sphinxdoc:Depends},
Description: feature flipper for Django (Documentation)
 Django Waffle is feature flipper for Django. You can define the conditions for
 which a flag should be active, and use it in a number of ways. Feature flags
 are a critical tool for continuously integrating and deploying applications.
 .
 Waffle aims to
  * provide a simple, intuitive API everywhere in your application;
  * cover common use cases with batteries-included;
  * be simple to install and manage;
  * be fast and robust enough to use in production; and
  * minimize dependencies and complexity.
 .
 This package contains the documentation.

Package: python3-django-waffle
Architecture: all
Depends:
 python3-django,
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python-django-waffle-doc,
Description: feature flipper for Django (Python3 version)
 Django Waffle is feature flipper for Django. You can define the conditions for
 which a flag should be active, and use it in a number of ways. Feature flags
 are a critical tool for continuously integrating and deploying applications.
 .
 Waffle aims to
  * provide a simple, intuitive API everywhere in your application;
  * cover common use cases with batteries-included;
  * be simple to install and manage;
  * be fast and robust enough to use in production; and
  * minimize dependencies and complexity.
 .
 This package contains the Python 3 version of the library.
